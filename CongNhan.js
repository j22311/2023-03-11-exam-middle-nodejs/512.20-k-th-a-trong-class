import { human } from "./Human.js";

class congnhan extends human {
    _nganhNghe;
    _noiLamViec;
    _luong;

    constructor(hoten,ngaysinh,quequan,nganhnghe,noilamviec,luong){
        super(hoten,ngaysinh,quequan);
        this.nganhnghe = nganhnghe;
        this.noilamviec = noilamviec;
        this._luong = luong;
    }
}

export {congnhan}