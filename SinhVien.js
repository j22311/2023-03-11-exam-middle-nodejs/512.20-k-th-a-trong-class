import { hocsinh } from "./HocSinh.js";

class sinhvien extends hocsinh {
    _chuyenNganh;
    _mSSV;

    constructor(hoten,ngaysinh,quequan,tentruong,lop,sdt,chuyennnganh,mssv){
        super(hoten,ngaysinh,quequan,tentruong,lop,sdt);
        this._chuyenNganh = chuyennnganh;
        this._mSSV = mssv;
    }
}
export {sinhvien}