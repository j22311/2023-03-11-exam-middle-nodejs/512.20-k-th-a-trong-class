import { human } from "./Human.js";

class hocsinh extends human {
    _tenTruong;
    _lop;
    _sdt;

    constructor(hoten,ngaysinh,quequan,tentruong,lop,sdt){
        super(hoten,ngaysinh,quequan);
        this._tenTruong = tentruong;
        this._lop = lop;
        this._sdt = sdt;
    }
}

export {hocsinh}